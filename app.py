import os
import sys
from TalkToTrello import TalkToTrello
from Card import Card
from driver import Driver


def __main__():
    key = os.getenv("KEY")
    token = os.getenv("TOKEN")
    if key == "" or token == "":
        print("I'm sorry, please update your .env file located in this directory. API calls are not possible without it.")  
    else:
        print("Welcome to TalkToTrello. A program that lets you add a card to a trello board. If you would like to quit, use 'q'")
        TrelloResponse = TalkToTrello(key, token)
        TrelloResponse.getUserID()
        boards = TrelloResponse.getBoards()
        TrelloResponse.continueAdding = True
        # While the user still wants to add cards, continue to add cards.
        # Defining the TalkToTrello in this task rather than the driver means that the UserID will stay constant while other data will get wiped out and also cuts down on the number of API calls.
        while TrelloResponse.continueAdding == True:
            addCard = Driver(TrelloResponse)
            addCard.startAdding()


            
        
        # add functionality for finally adding card
        # TrelloResponse.addCard(selectedList, "cardName", "weeeee", 'red')

if __name__ == "__main__":
    __main__()
