import os
import sys
import requests


class TalkToTrello:
    def __init__(self, key, token):
        self.allBoards = []
        self.boardLists = []
        self.allLabels = []
        self.userID = ""
        self.key = key
        self.token = token
        self.urlPrefix = "https://api.trello.com/1/"
        self.card_id = ""
        self.card_url = ""
        
    def getUserID(self):
        """
        will get the user ID using the provided key and token and assign it to the userID for the instance
        """
        url = self.urlPrefix + "members/me/?key=" + self.key + "&token=" + self.token
        getUser = requests.get(url)
        if getUser.status_code != 200:
            print("I'm sorry, it looks like your key and token may be incorrect. Please edit your .env file and try again.")
            sys.exit()
        else:
            self.userID = getUser.json()['id']
        # not getting boards here cause it's just a list of ids, no names


    def getBoards(self):
        """
        Using the users ID this will get the names of all the boards the user has access to.
        Assign the ones that have at least 1 list to the instances allBoards list.
        """
        url = self.urlPrefix + "members/" + self.userID + "/boards"
        queryparams = {"lists":"open", "key":self.key, "token":self.token}
        trelloBoards = requests.request("GET", url, params=queryparams)
        if trelloBoards.status_code != 200:
            print(trelloBoards.status_code)
        else:
            for board in trelloBoards.json():
                if board['lists'] != []:
                    board_name = board['name']
                    boardID = board['id']
                    self.allBoards.append((board_name, boardID))
        
    def getLists(self, boardID):
        """
        Given a board ID, get all of the available lists.
        """ 
        url = self.urlPrefix + "boards/" + boardID + "/lists"
        queryparams = {"key":self.key, "token":self.token}
        allLists = requests.request("GET", url, params=queryparams)
        if allLists.status_code != 200:
            print(allLists.status_code)
        else:
            for listed in allLists.json():
                listName = listed['name']
                listID = listed['id']
                self.boardLists.append((listName, listID))

    def getLabels(self, boardID):
        """
        Get the available labels (limited to 50 total) for the given board ID
        """
        url = self.urlPrefix + "/boards/" + boardID + "/labels"
        queryparams = {"fields":"all", "limit":"50", "key":self.key, "token":self.token}
        labels = requests.request("GET", url, params=queryparams)
        for label in labels.json():
            labelColor = label["color"]
            labelName = label["name"]
            labelID = label['id']
            self.allLabels.append((labelName, labelColor, labelID))


    def addCard(self, card_name, description, card_labels, listID, position="top"):
        """
        Actual API call that adds the card to the given list in the given board
        """
        url = self.urlPrefix + "cards"
        queryparams = {"name":card_name, "desc":description, "idLabels":card_labels, "idList":listID, "pos":position, "key":self.key, "token":self.token}
        addedCard = requests.request("POST", url, params=queryparams)
        if addedCard.status_code != 200:
            print(addedCard.status_code)
        else:
            self.card_url = addedCard.json()['shortUrl']
            self.card_id = addedCard.json()['id']


    def addComment(self, comment):
        """
        After a card is created, this adds a comment to the card, given the CardID
        """
        url = self.urlPrefix + "/cards/" + self.card_id + "/actions/comments"
        queryparams = {"text":comment, "key":self.key, "token":self.token}
        requests.request("POST", url, params=queryparams)
