import os
import sys
from TalkToTrello import TalkToTrello
from Card import Card

class Driver:
    """
    Class that gets all the card data from the user and then interacts with TalkToTrello.py to create the card online.
    This class has potential for refactor in the future to make it more efficient and encapsulated.
    """
    def __init__(self, TrelloResponse):
        self.adding = True
        self.boards = []
        self.boardFound = False
        self.CardDecision = False
        self.listPicked = False
        self.newCard = Card() # Instance of a new card
        self.TrelloResponse = TrelloResponse # Instance of TalkToTrello passed in from app.py - should probably refactor in the future
        self.comment = ""
        self.errorStatement = "I'm sorry, that isn't a valid input"
        self.continueAdding = ""


    def startAdding(self):
        """
        Kicks off the interaction with the user (provided their .env variables are updated)
        """
        self.chooseBoard()
        if self.boardFound == True:
            self.assembleCard()
        if self.CardDecision == True:
            self.chooseList()
        if self.listPicked == True:
            self.TrelloResponse.addCard(self.newCard.name, self.newCard.description, self.newCard.labelIDs, self.newCard.listID, position="top")
            if self.comment != "":
                self.TrelloResponse.addComment(self.comment)
            print("Your card has been added, here's a link: " + self.TrelloResponse.card_url)
            continueAdding = input("Would you like to add another card? press y to add another, any other key to quit: ")         
            if continueAdding.lower() == "y":
                self.TrelloResponse.continueAdding = True
            else:
                self.TrelloResponse.continueAdding = False

    def chooseBoard(self):
        """
        Loops through all the available boards, displaying them to the user.
        User selects board and function uses list indexing to select it and assign it to the Card instance.
        """
        while self.boardFound != True:
            print("These are the boards available for you to choose from, boards must have at lease 1 list to be included.")
            count = 1
            for key in self.TrelloResponse.allBoards:
                print(str(count) + " " + key[0])
                count += 1
            # get the number of the board from the user
            boardNumber = input("Please select a board by entering it's corresponding number here(or press 'q' to quit): ")
            if boardNumber == 'q' or boardNumber == 'Q':
                print("Goodbye")
                self.TrelloResponse.continueAdding = False
                break
            else:
                try:
                    boardNumber = int(boardNumber)
                    if int(boardNumber) < count and int(boardNumber) > 0:
                        boardInput = self.TrelloResponse.allBoards[(int(boardNumber)-1)]
                        self.newCard.boardID = boardInput[1]
                        self.boardFound = True
                    else:
                        print(self.errorStatement)
                except ValueError:
                    print(self.errorStatement)

    def chooseList(self):
        """
        Loops through the available lists, displaying them to the user, much the same as the board function above.
        This isn't exactly repeated code, but I could probably refactor the two so that I'm not repeating as much.
        """
        self.TrelloResponse.getLists(self.newCard.boardID)
        while self.listPicked != True:
            print("These are the lists available to add your card to.")
            count = 1
            for lis in self.TrelloResponse.boardLists:
                print(str(count) + " " + lis[0])
                count+=1 
            # get the number of the board from the user
            listNumber = input("Please select a list by entering it's corresponding number here(or press 'q' to quit): ")
            if listNumber == 'q' or listNumber == 'Q':
                print("Goodbye")
                self.TrelloResponse.continueAdding = False
                break
            else:
                try:
                    listNumber = int(listNumber)
                    if listNumber < count and listNumber > 0:
                        listInput = self.TrelloResponse.boardLists[(listNumber-1)]
                        self.newCard.listID = listInput[1]
                        self.listPicked = True
                    else:
                        print(self.errorStatement)
                except ValueError:
                    print(self.errorStatement)

    def assembleCard(self):
        """
        if a valid board selected, start the card construction process by getting the cards name and description.
        Calls the two functions asking if they want to add lables and comments.
        """  
        self.CardDecision = False       
        while self.CardDecision != True:
            UserWantsCard = input("Great, would you like to add a new card? y/n: ").upper()
            if UserWantsCard == "Y":
                print("Cool beans, let's work on adding a card.")
                self.newCard.name = input("Please enter your card title here: ")
                self.newCard.description = input("Please enter your description here: ")
                self.addLabels()
                self.getComment()
                self.CardDecision = True   
            elif UserWantsCard == "N" or UserWantsCard == "q":
                print("okay, see you later")
                self.TrelloResponse.continueAdding = False
                break
            else:
                print(self.errorStatement)



    def addLabels(self):
        """
        prints available labels names and colors, then gets their IDs and places them into a list, that then is turned into a string.
        """
        labelsAdded = False
        while labelsAdded != True:
            labelInput = input("Would you like to add labels? y/n: ")
            if labelInput.lower() == "y":
                userAdding = True
                labelsAvailable = self.TrelloResponse.getLabels(self.newCard.boardID)
                count = 1
                for label in self.TrelloResponse.allLabels:
                    print(str(count) + (' - Name: {:<8}       Color: {:<8} '.format(label[0], label[1])))
                    count+=1
                print("Enter the labels you want one at a time and enter 'x' when you've added all you want to add. ")
                # loop adding labels until the user stops. If they add 10 of the same label, the API doesn't care, but it could be good to code for not adding duplicates in the future.
                while userAdding == True:
                    inputNum = input("Enter Label number here:")
                    if inputNum == "x":
                        userAdding = False
                    else:
                        try:
                            inputNum = int(inputNum)
                            if inputNum < count and inputNum > 0:
                                labelID = self.TrelloResponse.allLabels[inputNum-1]
                                self.newCard.labels.append(labelID[2])
                                labelsAdded = True
                            else:
                                print(self.errorStatement)
                        except ValueError:
                            print(self.errorStatement)
                self.newCard.labelIDs = ",".join(self.newCard.labels)
            elif labelInput.lower() == "n":
                print("No labels will be added.")
                labelsAdded = True
            else:
                print(self.errorStatement)
        
    def getComment(self):
        """
        Just gets the data for a comment if the user decides to add one.
        """
        commenting = input("Would you like to add a comment? y/n: ")
        if commenting.lower() == "y":
            self.comment = input("Please enter your comment here: ")
        elif commenting.lower() == "n":
            print("No comment will be added")
        else:
            print(self.errorStatement)
    
