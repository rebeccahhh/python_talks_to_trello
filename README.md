# Python talks to Trello

This is a Python program that creates a card on a Trello board in a selected list with the option to add labels and comments.

### Running TalkToTrello
#### Requirements prior to running:
1. You must create a .env file with your Trello key and token. To obtain a trello key and token, you must have a trello account and [follow these instructions](https://developers.trello.com/v1.0/reference#introduction).
    - Click [this link](https://trello.com/app-key) to obtain your API key. Your Token you can also find a link to on that page.
2. You must install pipenv
    - Enter `pip install --user pipenv` into your CLI
        - for instructions unique to your image, check out their [Installation Guide](https://pipenv.readthedocs.io/en/latest/install/#installing-pipenv)
        - I elected to use pipenv to create the runtime environment so that all dependencies can be automatically installed.
        - For more info, checkout the pipenv documentation [here](https://pipenv.readthedocs.io/en/latest)
3. You must have Python 3.6 installed

#### Run instructions
1. Clone or for the repository here: [TalkToTrello](https://gitlab.com/rebeccahhh/python_talks_to_trello/tree/master)
2. Navigate on your machine to the project repository in your CLI(Command Line Interface)
3. Install pipenv if you have not already. Enter `pip install --user pipenv` into your CLI if you are using linux, if not, see the previous section for the pipenv installation guide.
4. Run the program by entering `pipenv run python app.py` into your CLI(Command Line Interface)



#### Known limitations\potential for enhancements
- boards without any lists are not listed, and no option is given to create a new list at this time
- input from user that does not fit 'utf-8' encoding may break the cli program. An language level issue that can be coded around in some instances, but was not a common enough error to do so for this take home test.
- there is some code that can be eliminated or abstracted. I have repeated code in the driver that given more time I would eliminate or rewrite.